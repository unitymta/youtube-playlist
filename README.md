# youtube-playlist Reactjs

Reactjs Tạo ứng dụng duyệt và quản lý các playlist từ Youtube có các tính năng sau:
Đăng nhập bằng account Google.
Dùng các thông tin cơ bản của account Google cho các thông tin của user: email, name, birthday... .
Cho phép thay đổi tên hiển thị.
Có thể logout được.
Chức năng tạo playlist.
Người dùng có thể tạo được các playlist khác nhau.
Mỗi playlist có thể chứa nhiều video.
Playlist có thể xoá được.
Playlist có thông tin cơ bản như: User, Title, Description...
Playlist được đánh dấu là private thì chỉ user đó mới xem được.
Playlist có thể xoá được bởi người tạo.
Khi add video vào playlist, người dùng có thể nhập link video từ Youtube. Thumb & Title của video sẽ tự động được lấy ra.
Chỉ có thể add tối đa 50 video vào playlist.
Chức năng player.
Người dùng có thể chọn được playlist để nghe.
Mặc định, chế độ video bị tắt, chỉ có thể nghe được.
Khi view, playlist có thêm option tắt/bật video player.
Màn hình Home.
Hiển thị public playlist từ user khác.
Hiển thị playlist từ user đang đăng nhập.
Yêu cầu:
Sử dụng Firebase để đăng nhập / lưu trữ dữ liệu.
Deploy sản phẩm lên Heroku.
