const initialState = {
  columns: [
    { title: "Author", field: "name" },
    { title: "Title", field: "title" },
    { title: "Description", field: "description" },
    { title: "Type", field: "category" },
    // { title: "Create date", field: "date" }
  ],
  lists: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case "GET_LISTS":
      return { ...state, lists: action.lists };
    default:
      return state;
  }
}
