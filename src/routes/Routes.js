import React, { Component } from "react";
import { Route } from "react-router-dom";
import Login from "../components/pages/login/index";
import Lists from "../components/pages/list/index";

class Routes extends Component {
  render() {
    return (
      <div className="wrapper">
        <Route path="/" exact component={Login} />
        <Route path="/login" exact component={Login} />
        <Route path="/lists" exact component={Lists} />
      </div>
    );
  }
}

export default Routes;
