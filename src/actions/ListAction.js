export function getLists(lists) {
    return {
        type: "GET_LISTS",
        lists
    };
}