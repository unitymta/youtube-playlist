import React, { Component } from "react";
import "./list.css";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import MaterialTable from "material-table";

class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lists: this.props.lists,
      user_id: localStorage.getItem("google_id"),
      data: []
    };
  }

  componentDidMount() {
    let lists = this.state.data;
    let user_id = this.state.user_id;
    console.log(lists, "lllllll", user_id);
    console.log(localStorage.getItem(`playlist${this.state.user_id}`));
    if (localStorage.getItem(`playlist${this.state.user_id}`)) {
      this.setState({
        data: JSON.parse(localStorage.getItem(`playlist${this.state.user_id}`))
      });
    }
  }

  onRowClick(event, rowData) {
    console.log(event, "dsdd", rowData.name);
    
    if (rowData.name) {
      console.log(event, "dsdd222", rowData.name);
      return (
        <Redirect
          to={{
            pathname: "/lists/detail",
            state: { name: "123" }
          }}
        />
      );
    }
  }

  render() {
    if (!localStorage.getItem("access_token")) {
      return <Redirect to="/" />;
    }

    return (
      <div>
        <MaterialTable
          title="Playlist"
          columns={this.props.columns}
          data={this.state.data}
          onRowClick={this.onRowClick}
          editable={{
            onRowAdd: newData =>
              new Promise(resolve => {
                setTimeout(() => {
                  resolve();
                  const data = [...this.state.data];
                  data.push(newData);
                  this.setState({ ...this.state, data });
                  localStorage.setItem(
                    `playlist${this.state.user_id}`,
                    JSON.stringify(this.state.data)
                  );
                }, 600);
              }),
            onRowUpdate: (newData, oldData) =>
              new Promise(resolve => {
                setTimeout(() => {
                  console.log("12122", resolve(), newData, oldData);
                  resolve();
                  const data = [...this.state.data];
                  data[data.indexOf(oldData)] = newData;
                  this.setState({ ...this.state, data });
                  localStorage.setItem(
                    `playlist${this.state.user_id}`,
                    JSON.stringify(this.state.data)
                  );
                }, 600);
              }),
            onRowDelete: oldData =>
              new Promise(resolve => {
                setTimeout(() => {
                  resolve();
                  const data = [...this.state.data];
                  data.splice(data.indexOf(oldData), 1);
                  this.setState({ ...this.state, data });
                  localStorage.setItem(
                    `playlist${this.state.user_id}`,
                    JSON.stringify(this.state.data)
                  );
                }, 600);
              })
          }}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    lists: state.ListReducer.lists,
    columns: state.ListReducer.columns
  };
}

export default connect(mapStateToProps, null)(List);
