import React from "react";
import "./login.css";
import GoogleLogin from "react-google-login";

function Login(props) {
  if (localStorage.getItem("access_token")) {
    props.history.push("/lists");
  }
  const responseGoogle = response => {
    console.log(response, "WWWWW2");
    if (response.accessToken) {
      localStorage.setItem("access_token", response.accessToken);
      localStorage.setItem("google_id",response.googleId);
      localStorage.setItem("user_name", response.profileObj.name);
      props.history.push("/lists");
    }
  };

  return (
    <div className="login">
      <GoogleLogin
        clientId="499426488230-aravmqbevkd2fosmjfp4c5fbs9gnrbaf.apps.googleusercontent.com"
        buttonText="LOGIN WITH GOOGLE"
        onSuccess={responseGoogle}
        onFailure={responseGoogle}
      />
    </div>
  );
}

export default Login;
