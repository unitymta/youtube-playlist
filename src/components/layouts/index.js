import React from "react";
import Header from "./Header.js";
import Sidebar from "./Sidebar.js";

function Layout() {
  return (
    <div>
      <Header />
      <Sidebar />
    </div>
  );
}

export default Layout;
