import React, { Component } from "react";
import DashboardIcon from '@material-ui/icons/Dashboard';
import PostAddIcon from '@material-ui/icons/PostAdd';
import PhotoLibraryIcon from '@material-ui/icons/PhotoLibrary';
import FeedbackIcon from '@material-ui/icons/Feedback';
import PersonIcon from '@material-ui/icons/Person';
import { NavLink } from "react-router-dom";
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

class Sidebar extends Component {
  render() {
    return (
      <div className="sidebar">
        <ul className="sidebar-list">
            <li className="sidebar-item">
                <NavLink className="sidebar-link" to="/dashboard" exact activeClassName="active">
                    <span className="sidebar-link__icon"><DashboardIcon/></span><span className="sidebar-link__text">Trang chủ</span>
                </NavLink>
            </li>
            <li className="sidebar-item">
                <NavLink className="sidebar-link" to="/lists" activeClassName="active">
                    <span className="sidebar-link__icon"><PostAddIcon/></span>
                    <span className="sidebar-link__text">Playlist</span>
                </NavLink>
            </li>
            <li className="sidebar-item">
                <NavLink className="sidebar-link" to="/library" exact activeClassName="active">
                    <span className="sidebar-link__icon"><PhotoLibraryIcon/></span><span className="sidebar-link__text">Thư viện</span>
                </NavLink>
            </li>
            <li className="sidebar-item">
                <NavLink className="sidebar-link" to="/feedback" exact activeClassName="active">
                    <span className="sidebar-link__icon"><FeedbackIcon/></span><span className="sidebar-link__text">Phản hồi</span>
                </NavLink>
            </li>
            <li className="sidebar-item">
                <NavLink className="sidebar-link" to="/user" exact activeClassName="active">
                    <span className="sidebar-link__icon"><PersonIcon/></span><span className="sidebar-link__text">Thông tin</span>
                </NavLink>
            </li>
        </ul>
      </div>
    );
  }
}

export default Sidebar;
