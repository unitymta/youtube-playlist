import React from "react";
import Grid from "@material-ui/core/Grid";
import AddIcon from "@material-ui/icons/Add";
import HomeIcon from "@material-ui/icons/Home";

function Header(props) {
  function logout() {
    if (localStorage.getItem("access_token")) {
      localStorage.removeItem("access_token");
      localStorage.removeItem("google_id");
      localStorage.removeItem("user_name");
      localStorage.removeItem(
        "oauth2_ss::https://explorer.apis.google.com::1::DEFAULT::_ss_"
      );
      localStorage.removeItem(
        "oauth2_ss::http://localhost:3000::1::DEFAULT::_ss_"
      );
      document.location.href = "/login";
    }
  }
  let user_name = localStorage.getItem("user_name");
  return (
    <Grid className="header-container" container>
      <Grid className="header-wrap" item xs={12} sm={6}>
        <a className="header-link" href="http://sangtx.com">
          <HomeIcon />
          sangtx.com
        </a>
        <div className="header-add">
          <AddIcon />
          <div className="header-add__btn">Mới</div>
        </div>
      </Grid>
      <Grid className="header-profile" item xs={12} sm={6}>
        <ul className="header-profile__list">
          <li className="header-profile__item">
            <a className="header-profile__link" href="/">
              Chào,{" "}
              <span className="display-name">
                {user_name}
              </span>
              <img
                alt="avatar"
                src="http://2.gravatar.com/avatar/b8aef47da44f0f381d1df49976acb8ae?s=26&d=mm&r=g"
                srcSet=""
                className=""
                height={26}
                width={26}
              />
            </a>
            <div className="header-profile__submenu">
              <ul className="submenu-list">
                <li className="submenu-item">
                  <a className="header-profile__link" href="/">
                    <img
                      alt="avatar"
                      src="http://2.gravatar.com/avatar/b8aef47da44f0f381d1df49976acb8ae?s=26&d=mm&r=g"
                      srcSet=""
                      className="avatar avatar-64 photo"
                      height={64}
                      width={64}
                    />
                    <span className="display-name">
                      {user_name}
                    </span>
                  </a>
                </li>
                <li className="submenu-item">
                  <a className="header-profile__link" href="/">
                    Chỉnh sửa thông tin
                  </a>
                </li>
                <li onClick={() => logout()} className="submenu-item">
                  <span className="header-profile__link" href="/">
                    Đăng xuất
                  </span>
                </li>
              </ul>
            </div>
          </li>
        </ul>
      </Grid>
    </Grid>
  );
}

export default Header;
