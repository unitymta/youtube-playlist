import React from "react";
import { Provider } from "react-redux";
import { createStore } from "redux";
import { BrowserRouter as Router } from "react-router-dom";
import RootReducer from "./reducers/RootReducer";
import Routes from "./routes/Routes";
import "./assets/styles/normalize.css";
import "./assets/styles/common.css";
import Layout from "./components/layouts";

function App() {
  const store = createStore(RootReducer);

  return (
    <Provider store={store}>
      <Router>
        <div className="app">
          <div className="content" id="content">
            <Layout />
            <Routes />
          </div>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
